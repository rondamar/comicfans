class StaticPagesController < ApplicationController
  def home
    @comics = Comic.all
  end

  def about


  end

  def browse

  end

  def mycomics
    @comic = current_user.comics.build if logged_in?
  end
end
