class ComicsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy


  def index
    @comics = Comic.all
  end

  def show
    @comic = Comic.find(params[:id])
    @pages = @comic.pages.paginate(page: params[:page])

    @page = @comic.pages
    @page = @comic.pages.build if logged_in?

  end

  def create
    @comic = current_user.comics.build(comic_params)
    if @comic.save
      flash[:success] = "Comic created!"
      redirect_to mycomics_url
    else
      render 'static_pages/mycomics'
    end
  end

  def destroy
    @comic.destroy
    flash[:success] = "Comic deleted"
    redirect_to request.referrer || root_url

  end

  private

  def comic_params
    params.require(:comic).permit(:title, :image, :summary)
  end

  def correct_user
    @comic = current_user.comics.find_by(id: params[:id])
    redirect_to root_url if @comic.nil?
  end
end
