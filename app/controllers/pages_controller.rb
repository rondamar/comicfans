class PagesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  before_filter :load_comic


  def index
    @pages = Page.all

  end

  def new
    @page = @comic.pages.new
  end

  def show
    @page = Page.find(params[:id])
  end

  def create
    @page = @comic.pages.new(page_params)

    respond_to do |format|

    if @page.save
      format.html { redirect_to [@comic, @page], notice: 'Page was successfully created.'}

    else
      render 'new'
    end
      end
  end

  def destroy
    @page.destroy
    flash[:success] = "Page deleted"
    redirect_to request.referrer || root_url

  end

  private

  def load_comic
    @comic = Comic.find(params[:comic_id])
  end

  def page_params
    params.require(:page).permit(:image, :summary, :comic_id)
  end

  def correct_user
    @page = current_user.comics.pages.find_by(id: params[:id])
    redirect_to root_url if @page.nil?
  end
end
