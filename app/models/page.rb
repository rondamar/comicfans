class Page < ActiveRecord::Base
  attr_accessor :comic_id
  belongs_to :user
  belongs_to :comic

  default_scope -> { order(created_at: :desc) }
  mount_uploader :image, ImageUploader

  validates :comic_id, presence: true


end
