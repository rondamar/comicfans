class Comic < ActiveRecord::Base
  belongs_to :user
  has_many :pages, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  mount_uploader :image, ImageUploader
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 50 }
  accepts_nested_attributes_for :pages


end
