class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :summary
      t.references :user, index: true, foreign_key: true
      t.references :comic, index: true, foreign_key: true
      t.integer :number
      t.string :image

      t.timestamps null: false
    end
    add_index :pages, [:user_id, :created_at]

  end
end
