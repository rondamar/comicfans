class CreateComics < ActiveRecord::Migration
  def change
    create_table :comics do |t|
      t.text :summary
      t.text :title
      t.string :genre
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :comics, [:user_id, :created_at]
  end
end
